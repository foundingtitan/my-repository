import { useState, useEffect, useContext } from 'react';

//bootstrap
import { Container } from 'react-bootstrap';

//components
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

//React Context
import UserContext from '../UserContext';


export default function Products() {
    const { user } = useContext(UserContext);

    const [allProducts, setAllProducts] = useState([])

    const fetchData = () => {
        fetch('http://localhost:4000/products/all')
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setAllProducts(data)
            })
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <Container>
            {
                (user.isAdmin === true) ?
                    <AdminView productsData={allProducts} fetchData={fetchData} />
                    :
                    <UserView productsData={allProducts} />
            }
        </Container>
    )
}

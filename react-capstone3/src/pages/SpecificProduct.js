import { useState, useEffect, useContext } from 'react'
import { Container, Card, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Link, useHistory, useParams } from 'react-router-dom'

import UserContext from '../UserContext'



export default function SpecificProduct() {

    const { user } = useContext(UserContext)

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)


    //useParams() contains any values we are trying to pass in the URL stored in a key/value pair
    //useParams is how we receive the courseId passed via the URL
    const { productId } = useParams()



    useEffect(() => {
        fetch(`http://localhost:4000/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)

            })
    }, [])


    const buy = (productId) => {
        fetch(`http://localhost:4000/users/buy/${productId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },

            body: JSON.stringify({
                productId: `${productId}`
            })
        }
        )


            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    Swal.fire({
                        title: 'Huzzah!!',
                        icon: 'success',
                        text: `You have successfully purchased ${name}! 
                        Product is now sent to your registered e-mail.`

                    })
                } else {
                    Swal.fire({
                        title: 'Oooops',
                        icon: 'error',
                        text: 'Something went wrong. Please try again.'
                    })
                }
            })
    }



    return (
        <Container fluid style={{ backgroundColor: "#EFEFE9", height: "100vh", paddingTop: 120 }}>
            <Card style={{ backgroundColor: "#EFEFE9" }}>
                <Card.Header className=" text-white text-center pb-0" style={{ backgroundColor: "#258A8A" }} >

                    <h4>Review your purchase</h4>
                </Card.Header>

                <Card.Body>
                    <Card.Text>
                        <strong>Product Name: </strong> {name}
                    </Card.Text>
                    <Card.Text>
                        <strong>Product Description:</strong> {description}
                    </Card.Text>
                    <strong>Price:</strong> PHP {price}
                </Card.Body>


                <Card.Footer>
                    {
                        user.accessToken !== null ?
                            <Button style={{ color: "black", borderColor: "#F4D53C", backgroundColor: "#F4D53C", marginBottom: 8 }} block onClick={() => buy(productId)}>Checkout for PHP {price}</Button>
                            :
                            <Link className="btn btn-warning btn-block" to="/login">Login to Purchase</Link>
                    }


                </Card.Footer>
            </Card>


        </Container >


    )
}



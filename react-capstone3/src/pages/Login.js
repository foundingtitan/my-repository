// Email has been verified! Welcome back
import { Fragment, useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'

//React Context
import UserContext from '../UserContext'

//Routing
import { Redirect, useHistory } from 'react-router-dom';


export default function Login() {

    //The useHistory hook gives you access to the history instance that you may use to navigate or to access the location
    const history = useHistory();

    //useContext is a react hook used to unwrap our context. It will return the data passed as values by a provider (UserContext.Provider component in App.js)
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [loginButton, setLoginButton] = useState(false);

    // console.log(email)
    // console.log(password)


    useEffect(() => {


        if (email !== '' && password !== '') {
            setLoginButton(true);

        } else {
            setLoginButton(false);
        }

    }, [email, password])


    function loginUser(e) {
        e.preventDefault();

        //fetch has 2 arguments
        //1. URL from the server(routes)
        //2. optional object which contains additional information about our requests such as method, headers, body, etc.
        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json()) //for parsing of token
            .then(data => {

                console.log(data)

                if (data.accessToken !== undefined) {
                    localStorage.setItem('accessToken', data.accessToken);
                    setUser({ accessToken: data.accessToken });
                    // localStorage.setItem('email', data.email);
                    // setUser({ email: email })

                    Swal.fire({
                        title: 'Yaaaaay',
                        icon: 'success',
                        text: `${email} has been verified! Welcome back!`
                    }
                    )

                    //get user's details from our token
                    fetch('http://localhost:4000/users/details', {
                        headers: {
                            Authorization: `Bearer ${data.accessToken}`
                        }
                    })
                        .then(res => res.json()) //for parsing of all user details
                        .then(data => {
                            console.log(data)
                            if (data.isAdmin === true) {
                                localStorage.setItem('email', data.email)
                                localStorage.setItem('isAdmin', data.isAdmin)

                                setUser({
                                    email: data.email,
                                    isAdmin: data.isAdmin
                                })

                                //If admin, redirect the page to /courses
                                //push(path) - pushes a new entry to onto the history stack
                                history.push('/products/all')
                            } else {
                                // localStorage.setItem('accessToken', data.accessToken)
                                //If not admin, redirect to homepage
                                history.push('/')
                            }

                        })

                } else {
                    Swal.fire({
                        title: 'Oooops!',
                        icon: 'error',
                        text: 'Something went wrong. Check your credentials'
                    })
                }
                setEmail('')
                setPassword('')
            })



        //local storage allows us to save data within our browsers as strings
        //the setItem() method of the Storage interface, when passed a key name and value, will add that key to the given storage object, or update the key's value if it already exists
        //setItem('key', value)

    }

    //Create a conditional statement that will redirect the user to the homepage when a user is logged in.

    return (

        (user.accessToken !== null) ?
            <Redirect to="/" />
            :



            <Fragment>

                <div style={{ padding: 120, height: "100vh" }}>


                    <Form className="block-example border-top border-bottom border-danger" style={{ padding: 40 }} onSubmit={(e) => loginUser(e)}>
                        <h1 className="text-center" style={{ paddingBottom: 20 }}><strong>Log-in</strong></h1>
                        <Form.Group >
                            <Form.Label>Email address:</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />

                        </Form.Group>
                        <Form.Group>
                            <Form.Label> Password:</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Enter your password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {loginButton ?
                            <Button block style={{ backgroundColor: "#F4D53C", border: "#F4D53C", color: "black" }} type="submit" id="submitBtn">
                                <strong>Submit</strong>
                            </Button>
                            :
                            <Button block variant="dark" type="submit" id="submitBtn" disabled>
                                <strong>Submit</strong>
                            </Button>
                        }

                    </Form>
                </div>
            </Fragment >
    )
}



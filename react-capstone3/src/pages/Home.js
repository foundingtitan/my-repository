
import { Fragment, useContext } from 'react';
import { Image, Button, Carousel, Container } from 'react-bootstrap';
// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';
// import Welcome from '../components/Welcome'
import car from './gumroad.jpeg'


import UserContext from '../UserContext'
export default function Home() {
    const { user } = useContext(UserContext)

    let landing = (user.accessToken !== null) ?
        <Fragment>

            <div style={{ paddingTop: 80, marginLeft: 420, width: "610px" }}>

                <Button href="/products/all" style={{ height: "80px", width: "200px", backgroundColor: "#F4D53C", border: "#F4D53C", color: "black" }}>
                    <h1> Browse</h1>
                </Button>{' '}

            </div>


        </Fragment>
        :
        <Fragment>
            <div style={{ paddingTop: 80, marginLeft: 230, width: "610px" }}>
                <Button href="/register" style={{ height: "80px", width: "200px", backgroundColor: "#E83314", border: "#E83314", color: "black" }}>
                    <h1> Register</h1>
                </Button>{' '}
                <Button href="/login" style={{ height: "80px", width: "200px", backgroundColor: "#F4D53C", border: "#F4D53C", color: "black" }}>
                    <h1> Login</h1>
                </Button>{' '}
                <Button href="/products/all" style={{ height: "80px", width: "200px", backgroundColor: "#E83314", border: "#E83314", color: "black" }}>
                    <h1> Browse</h1>
                </Button>{' '}
            </div>
        </Fragment>



    return (

        <Fragment>


            <Container fluid style={{ backgroundColor: "#EFEFE9", height: "100vh" }}>

                {/*  <Image src="https://via.placeholder.com/1000" fluid />  */}

                < h1 class="text-center" style={{ paddingTop: 150, paddingBottom: 150, fontSize: 100, backgroundColor: "#96BFBF" }}> Find the best digital products in one place!</h1 >
                {landing}


            </Container>





        </Fragment >


    )
}
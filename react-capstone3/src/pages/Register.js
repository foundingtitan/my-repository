
import { Fragment, useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext'

export default function Register() {

    const history = useHistory();

    const { user, setUser } = useContext(UserContext)

    //State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password, setPassword] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);

    //To check if values are successfully binded
    console.log(firstName)
    console.log(lastName)
    console.log(email)
    console.log(mobileNo)
    console.log(password)
    console.log(password1)
    console.log(password2)


    useEffect(() => {
        //Validation to enable the submit button when all fields are populated and both passwords match

        if ((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (mobileNo.length >= 11) && (password1 === password2)) {
            setIsActive(true);

        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2])


    // function registerUser(e){
    function registerUser(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/checkEmail', {
            method: 'POST',
            headers: {
                'Accept': 'application/json,  text/plain',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === true) {

                    Swal.fire({
                        title: 'Duplicate e-mail found',
                        icon: 'warning',
                        text: `${email} already exists.`
                    })

                } else {

                    fetch('http://localhost:4000/users/register', {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json,  text/plain',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            mobileNo: mobileNo,
                            password: password1
                            // password1: password1,
                            // password1: password2
                        })
                    })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data)

                            if (data === true) {
                                Swal.fire({
                                    title: 'Yaaaaay',
                                    icon: "success",
                                    text: "You have successfully registered"
                                })
                                history.push("/login")
                            } else {
                                Swal.fire({
                                    title: 'Oooops',
                                    icon: "error",
                                    text: "Failed to register"
                                })
                            }


                        })

                }
            })


        //To clear out the data in our input fields
        setFirstName('');
        setLastName('');
        setEmail('')
        setMobileNo('')
        setPassword('')
        setPassword1('')
        setPassword2('')

        // Swal.fire({
        // 	title: 'Yaaaaay',
        // 	icon: 'success',
        // 	text: 'You have successfully registered'
        // }
        // 	)

        // alert('Thank you for registering')
    }


    //Two way binding
    //THe values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is called two way binding
    //The data we cahanged in the view has updated the state
    //The data in the state has updated the view


    /*
    Mini-Activity:

    - Redirect the user back to "/" (homepage) route if a user is already logged in

    */

    return (
        (user.accessToken !== null) ?
            <Redirect to="/" />
            :

            <Fragment>
                <div style={{ padding: 120 }}>

                    <Form className="block-example border-top border-bottom border-danger" style={{ padding: 40 }} onSubmit={(e) => registerUser(e)}>
                        <h1 class="text-center" style={{ paddingBottom: 20 }}><strong>Create an account</strong></h1>
                        <Form.Group>
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="First Name"
                                value={firstName}
                                onChange={e => setFirstName(e.target.value)} //the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
                                required
                            />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Last Name"
                                value={lastName}
                                onChange={e => setLastName(e.target.value)} //the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
                                required
                            />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Email address:</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={e => setEmail(e.target.value)} //the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
                                required
                            />

                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Mobile"
                                value={mobileNo}
                                onChange={e => setMobileNo(e.target.value)} //the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
                                required
                            />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label> Password:</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Enter your password"
                                value={password1}
                                onChange={e => setPassword1(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label> Verify Password:</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Verify your password"
                                value={password2}
                                onChange={e => setPassword2(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/*<Form.Group>
                <Form.Label> Verify Password:</Form.Label>
                <Form.Control 
                    type = "password"
                    placeholder = "Verify your password"
                    value = {password}
                    onChange = {e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>*/}
                        {isActive ?
                            <Button block variant="primary" style={{ backgroundColor: "#F4D53C", border: "#F4D53C", color: "black" }} type="submit" id="submitBtn">
                                <strong>Submit</strong>
                            </Button>
                            :
                            <Button block variant="light" type="submit" id="submitBtn" disabled>
                                <strong>Submit</strong>
                            </Button>
                        }

                    </Form>
                </div>
            </Fragment>
    )
}
import { useState, useEffect, useContext } from 'react';

//bootstrap
import { Container } from 'react-bootstrap';

//components
import View from '../components/View';


//React Context
import UserContext from '../UserContext';


export default function Orders() {
    const { user } = useContext(UserContext);

    const [allOrders, setAllOrders] = useState([])

    const fetchData = () => {
        fetch('http://localhost:4000/orders/')
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setAllOrders(data)
            })
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <Container>
            {
                (user.isAdmin === true) ?
                    <View ordersData={allOrders} fetchData={fetchData} />
                    :
                    <View ordersData={allOrders} fetchData={fetchData} />
            }
        </Container>
    )
}

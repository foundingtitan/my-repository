import { useState, useEffect } from 'react' //to use for Fragment parent tag
import './App.css';
import AppNavbar from './components/AppNavbar'

//Transferred to Home.js
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import ProductCard from './components/ProductCard'

//pages
import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import SpecificProduct from './pages/SpecificProduct'
import Orders from './pages/Orders'

//routing components
import { BrowserRouter as Router } from 'react-router-dom' //enables to simulate/synchronize all pages with URL
import { Route, Switch } from 'react-router-dom' //switch is component that declares routes

//Bootstrap
import { Container } from 'react-bootstrap'

// //React Context
import UserContext from './UserContext';



/*
BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser

Switch component then declares with Route we can go to

Route component will render components within the switch container based on the defined route

Exact property disables the partial matching for a route and makes sure that it only returns the route if the path is an exact match to the current URL

If exact and path is missing, the Route component will make it undefined route and will be loaded into a specified component/
*/


/*
React Context is a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested. Context helps you to broadcast the data and changes happening to that data to all components.
*/


function App() {

  //add a state hook for user,
  //the getItem() method returns value of the specified Storage Object item
  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  });

  //Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  //Optional: Used to check if the user information is properly stored upon login and the localstorage info is cleared upon logout
  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  }, [user])


  //Provider Component that allows consuming components to subscribe to context changes
  return (

    //React.Fragment is a parent tag that is used as container for multiple components
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <Router>

        < AppNavbar />

        <Container fluid style={{ backgroundColor: "#EFEFE9", height: "100%" }}>
          <Container >

            <Switch>
              < Route exact path="/" component={Home} />
              <Route exact path="/products/all" component={Products} />
              <Route exact path="/orders/" component={Orders} />
              <Route exact path="/products/:productId" component={SpecificProduct} />
              {/* <Route exact path="/users/purchase/:productId" component={SpecificProduct} /> */}
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route path="/" component={Error} />
              {/*or simply <Route component = {Error} />*/}
            </Switch>
          </Container>


        </Container>


      </Router >
    </UserContext.Provider >
  );
}

export default App;


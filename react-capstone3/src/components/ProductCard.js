import { useState, useEffect, Fragment } from 'react';
import { Card, Button, Row, Col, ListGroupItem, ListGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { CardDeck, Container, CardGroup } from 'react-bootstrap'

export default function ProductCard({ productProp }) {

    //Checks to see if the data was successfully passed
    //console.log(props)
    //console.log(typeof props)

    //Deconstruct the course properties into their own variables(destructuring)
    const { _id, name, description, price } = productProp;

    //Use the state hook for this component to be able to store its state
    //States are used to keep track of information related to individual component
    //Syntax
    //const [getter, setter] = useState(initialGetterValue)
    //getter = stored initial(default value)
    //setter = updated value
    //const [count, setCount] = useState(0)
    //const [seats, setSeats] = useState(10)
    //state hook that indicates the button for enrollment
    //const [isOpen, setIsOpen] = useState(true);

    /*useEffect(()=>{
        if(seats === 0){
            setIsOpen(false);
        }
    }, [seats])
*/
    //console.log(useState(0))

    /*	function enroll() {
            if (seats > 0){
                setCount(count + 1);
                console.log('Enrollees: ' + count);
                setSeats(seats - 1);
                console.log('Seats: ' + seats);
            } else {
                alert("No more seats available")
            }
        }*/

    return (
        // <Card>
        //     <Card.Body>
        //         <Card.Title><h2>{name}</h2></Card.Title>
        //         {/*<Card.Subtitle>Description:</Card.Subtitle>
        //             <Card.Text>{description}</Card.Text>
        //             <Card.Subtitle>Price:</Card.Subtitle>
        //             <Card.Text>Php {price}</Card.Text>*/}
        //         {/*<Card.Text>Enrollees: {count}</Card.Text>
        //             <Card.Text>Seats: {seats}</Card.Text>*/}

        //         <Link className="btn btn-primary" to={`/products/${_id}`}> Details </Link>

        //     </Card.Body>
        // </Card>


        //https://via.placeholder.com/100
        // <Fragment>
        //     <Container fluid>
        //         {/* <CardDeck style={{ display: 'flex', flexDirection: 'row', justifyContent: "center" }}> */}
        //         <Row style={{ backgroundColor: "black", width: 1200, marginTop: 50 }}>
        //             <Card >
        //                 {/* <Card style={{ width: '17rem', padding: 4, margin: 4 }}> */}
        //                 <Card.Img variant="top" src="https://via.placeholder.com/100" />
        //                 <Card.Title style={{ margin: 4 }}>{name}</Card.Title>
        //                 <ListGroup className="list-group" >
        //                     <ListGroupItem style={{ padding: 2, borderColor: "white" }}>{price}</ListGroupItem>
        //                     <ListGroupItem style={{ padding: 4, borderColor: "white" }}>Favorite Button // How many sold</ListGroupItem>
        //                     <ListGroupItem style={{ padding: 2, borderColor: "white" }}>  <Button variant="warning">Add to Cart </Button><Button variant="success">Checkout </Button><Link className="btn btn-primary" to={`/products/${_id}`}> Specific </Link></ListGroupItem>
        //                 </ListGroup>
        //             </Card>
        //             {/* </CardDeck> */}


        //         </Row>
        //     </Container>
        // </ Fragment >
        <Fragment>
            {/* <h1 class="text-center" style={{ paddingTop: 20 }}> Products </h1> */}
            <Container style={{ bacgroundColor: "#EFEFE9" }}>

                <CardGroup style={{ paddingTop: 20, paddingBottom: 20 }}>
                    <Row xs={1} md={4} className="g-12">

                        <Card style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 5, backgroundColor: "#EFEFE9" }}>
                            <Card.Img variant="top" src="https://via.placeholder.com/150/72B4B4/FFFFFF/?text=Image Here" />
                            <Card.Body>
                                <Card.Title >Product Name</Card.Title>
                                <Card.Text>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                </Card.Text>
                            </Card.Body>
                            <ListGroup className="list-group" >

                                <Link className="btn" style={{ backgroundColor: "#F4D53C", marginBottom: 8 }} to={`/products/${_id}`}> Buy Now </Link>

                            </ListGroup>
                        </Card>
                        <Card style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 5, backgroundColor: "#EFEFE9" }}>
                            <Card.Img variant="top" src="https://via.placeholder.com/150/72B4B4/FFFFFF/?text=Image Here" />
                            <Card.Body>
                                <Card.Title>Product Name</Card.Title>
                                <Card.Text>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                </Card.Text>
                            </Card.Body>
                            <ListGroup className="list-group" >

                                <Link className="btn" style={{ backgroundColor: "#F4D53C", marginBottom: 8 }} to={`/products/${_id}`}> Buy Now </Link>

                            </ListGroup>
                        </Card>
                        <Card style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 5, backgroundColor: "#EFEFE9" }}>
                            <Card.Img variant="top" src="https://via.placeholder.com/150/72B4B4/FFFFFF/?text=Image Here" />
                            <Card.Body>
                                <Card.Title>Product Name</Card.Title>
                                <Card.Text>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                </Card.Text>
                            </Card.Body>
                            <ListGroup className="list-group" >

                                <Link className="btn" style={{ backgroundColor: "#F4D53C", marginBottom: 8 }} to={`/products/${_id}`}> Buy Now </Link>

                            </ListGroup>
                        </Card>
                        <Card style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 5, backgroundColor: "#EFEFE9" }}>
                            <Card.Img variant="top" src="https://via.placeholder.com/150/72B4B4/FFFFFF/?text=Image Here" />
                            <Card.Body>
                                <Card.Title>{name}</Card.Title>
                                <Card.Text>
                                    {description}
                                </Card.Text>
                            </Card.Body>
                            {/* <Card.Footer> */}
                            <ListGroup className="list-group" >

                                <Link className="btn" style={{ backgroundColor: "#F4D53C", marginBottom: 8 }} to={`/products/${_id}`}> Buy Now </Link>

                            </ListGroup>
                            {/* <small className="text-muted">Last updated 3 mins ago</small> */}

                            {/* </Card.Footer> */}
                        </Card>
                    </Row>

                </CardGroup >

            </Container>
        </Fragment>



    )
}


//Check if the CourseCard Component is getting the correct prop types
//Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
ProductCard.propTypes = {
    //The 'shape' method is used to check if a prop object conforms to a specific 'shape'
    productProp: PropTypes.shape({
        //define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

/*
mounting > updating > unmounting
mounting > rendering > re rendering > unmounting

login page (mounting)> typing data in inputs (rendering) (re rendering) > home (unmounting)

*/

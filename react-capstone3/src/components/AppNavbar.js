// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

import { Navbar, Nav, Badge } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import { Fragment, useContext } from 'react'

//React Context
import UserContext from '../UserContext'


export default function AppNavbar() {

    const { user } = useContext(UserContext)
    console.log({ user })

    let rightNav = (user.accessToken !== null) ?
        <Fragment>

            <Nav.Link href="/orders/" style={{ color: "white" }}>My Orders</Nav.Link>
            <Nav.Link href="/logout" style={{ color: "white" }}>Logout</Nav.Link>


        </Fragment>
        :
        <Fragment>
            {/* <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link> */}
            <Nav.Link href="/login" style={{ color: "white" }}>Login</Nav.Link>
            <Nav.Link href="/register" style={{ color: "white" }}>Register</Nav.Link>
        </Fragment>


    return (

        user.isAdmin !== true ?
            <Navbar style={{ backgroundColor: "#258A8A" }} expand="lg">
                <Navbar.Brand style={{ color: "white" }} as={Link} to="/"><strong><u>Zuitt Shop</u></strong></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {/* <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/products/all">Products</Nav.Link> */}
                        <Nav.Link href="/" style={{ color: "white" }}>Home</Nav.Link>
                        <Nav.Link href="/products/all" style={{ color: "white" }}>Products</Nav.Link>

                    </Nav>

                    <Nav className="ml-auto" >
                        {rightNav}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            :
            <Navbar style={{ backgroundColor: "#258A8A" }} expand="lg">
                <Navbar.Brand style={{ color: "white" }} as={Link} to="/"><strong><u>Zuitt Shop</u></strong></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {/* <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} to="/products/all">Products</Nav.Link> */}
                        {/* <Nav.Link href="/" style={{ color: "white" }}>Home</Nav.Link> */}
                        {/* <Nav.Link href="/products/all" style={{ color: "white" }}>Products</Nav.Link> */}

                    </Nav>

                    <Nav className="ml-auto" >
                        <Fragment>

                            {/* <Nav.Link href="/orders/" style={{ color: "white" }}>My Orders</Nav.Link> */}
                            <Nav.Link href="/logout" style={{ color: "white" }}>Logout</Nav.Link>


                        </Fragment>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
    )
}


//Link is used to navigate in general
//NavLink is used if you want to add styles to your link






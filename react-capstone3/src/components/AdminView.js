import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props) {

    const { productsData, fetchData } = props
    console.log(props)

    const [products, setProducts] = useState([])

    //Add state for addCourse
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    //States to our modals if it needs to open/close
    const [showAdd, setShowAdd] = useState(false);
    //States for our editCourse modals
    const [showEdit, setShowEdit] = useState(false);

    //Add a state for courseId for the fetch URL
    const [productId, setProductId] = useState('')

    //Functions to handle opening and closing our Add New Course Modal
    const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);


    //Function to handle closing in our Edit Course Modal. We need to reset all relevant states back to their default values.
    const closeEdit = () => {
        setShowEdit(false)
        setName('')
        setDescription('')
        setPrice(0)
    }

    const openEdit = (productId) => {
        fetch(`http://localhost:4000/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                //Populate all input values with the course information that we fetched
                setProductId(data._id)
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
            })

        //Then, open the modal
        setShowEdit(true)
    }



    useEffect(() => {
        const productsArr = productsData.map(product => {
            return (
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>{product.price}</td>
                    <td className={product.isActive ? "text-success" : "text-danger"}>
                        {product.isActive ? "Available" : "Unavailable"}
                    </td>
                    <td>
                        <Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>

                        {product.isActive ?
                            <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
                            :
                            <Button variant="success" size="sm" onClick={() => activateToggle(product._id, product.isActive)}>Enable</Button>
                        }

                    </td>
                </tr>

            )
        })
        setProducts(productsArr)
    }, [productsData])


    //Add New Product function
    const addProduct = (e) => {
        e.preventDefault();
        fetch('http://localhost:4000/products/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data === true) {
                    //Run our fetchData function that we passed from our parent component, in order to re-render our page
                    fetchData()

                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: 'Product successfully added.'
                    })

                    setName('')
                    setDescription('')
                    setPrice(0)

                    //automatic close of our modal
                    closeAdd()
                } else {
                    Swal.fire({
                        title: 'Something Went Wrong!',
                        icon: 'error',
                        text: 'Please Try Again.'
                    })
                }
            })
    }


    //Edit Product Function
    const editProduct = (e, productId) => {
        e.preventDefault();//prevents the default behavior of refreshing the page

        fetch(`http://localhost:4000/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    fetchData()
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: 'Product successfully updated'
                    })
                    closeEdit()
                } else {
                    fetchData()
                    Swal.fire({
                        title: 'Something Went Wrong!',
                        icon: 'error',
                        text: 'Please try again'
                    })
                }
            })
    }


    //Archive/Disable a product
    const archiveToggle = (productId, isActive) => {
        fetch(`http://localhost:4000/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    fetchData()
                    Swal.fire({
                        title: 'Success',
                        icon: 'success',
                        text: 'Product successfully disabled'
                    })
                } else {
                    fetchData()
                    Swal.fire({
                        title: 'Something Went Wrong',
                        icon: 'error',
                        text: 'Please Try again'
                    })
                }
            })
    }


    //Active/ Enable Button
    const activateToggle = (productId, isActive) => {
        fetch(`http://localhost:4000/products/${productId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    fetchData()
                    Swal.fire({
                        title: 'Success',
                        icon: 'success',
                        text: 'Product successfully activated'
                    })
                } else {
                    fetchData()
                    Swal.fire({
                        title: 'Something Went Wrong',
                        icon: 'error',
                        text: 'Please Try again'
                    })
                }
            })
    }

    return (

        < Fragment >

            <div className="text-center my-4">
                <h2>Admin Dashboard</h2>
                <div className="d-flex justify-content-center">
                    <Button variant="primary" onClick={openAdd}> Add New Product </Button>
                </div>
            </div>
            <Table striped bordered hover responsive fluid>
                <thead className="bg-dark text-white">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {products}
                </tbody>
            </Table>

            {/*ADD NEW PRODUCT MODAL*/}

            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={e => addProduct(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name:</Form.Label>
                            <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Price:</Form.Label>
                            <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
                        </Form.Group>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>


            {/*Edit Product Modal*/}
            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editProduct(e, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update Product</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name:</Form.Label>
                            <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Price:</Form.Label>
                            <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
                        </Form.Group>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>





        </Fragment >

    )
}

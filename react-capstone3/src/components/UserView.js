import { useState, useEffect, Fragment } from 'react';
import ProductCard from './ProductCard';


export default function UserView({ productsData }) {
    const [products, setProducts] = useState([])
    console.log(productsData)


    useEffect(() => {

        const productsArr = productsData.map(product => {
            //only render active courses
            if (product.isActive === true) {
                return (
                    < ProductCard productProp={product} key={product._id} />
                )
            } else {
                return null;
            }
        })
        //set the courses state to the result of our map function, to bring our returned course components outside of the scope of our useEffect where our return statement below can see it
        setProducts(productsArr)

    }, [productsData])

    return (
        <Fragment>
            {products}
        </Fragment>

    )
}
